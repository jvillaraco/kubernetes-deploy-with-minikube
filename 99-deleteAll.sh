# 99-deleteAll.sh

kubectl get all,pv,pvc

kubectl get all,pv,pvc --namespace app

echo "Deleteing app namespace and everything inside ..."
echo "-------------------------------------------------"
kubectl delete namespace app
echo; echo

echo "Deleting mysql-pv.volume ..."
echo "----------------------------"
kubectl delete persistentvolume/mysql-pv-volume
echo; echo

echo "List everything in app namespace"
echo "--------------------------------"
kubectl get all,pv,pvc,ingress --namespace app
echo; echo

echo "List everything in default namespace"
echo "------------------------------------"
kubectl get all,pv,pvc,ingress
echo; echo

kubectl get namespaces
