select '## Show databases ##';
show databases;

select '## Database creation ##';
create database flask_app;

select '## Create db user ##';
create user user_flask_app identified by 'secret';

select '## Grant all to user on flask_app db ##';
grant all on flask_app.* to user_flask_app;

