mariadb/readme.md
=================
** Scripts and doc related to mariadb service and data handle **

# Interacting to mariadb dockered services
## Execute local host script to dockered mariadb

```kubectl exec -i pod/mariadb-694f94fcdb-5lj6c -- /bin/sh -c 'mysql -u root -ppassword --skip-column-names' < mariadb/creation.sql```
```
## Show databases ##
information_schema
mysql
performance_schema
sys
## Database creation ##
## Create db user ##
## Grant all to user on flask_app db ##
```

```kubectl exec -i pod/mariadb-694f94fcdb-5lj6c -- /bin/sh -c 'mysql -u user_flask_app -psecret --skip-column-names' < mariadb/selectDatabaseTime.sql```
```
2021-10-01 18:08:04
```

## Testing PV after minikube restart
After minikube delete + start next operation is executed to confirm database strcuture and user is created:
```kubectl exec -i pod/mariadb-6477d969b4-hnmgk -- /bin/sh -c 'mysql -u user_flask_app -psecret --skip-column-names' < mariadb/selectDatabaseTime.sql```
```
2021-10-03 17:47:29
```

minikube stop:
```minikube stop```
```
✋  Stopping node "minikube"  ...
🛑  Powering off "minikube" via SSH ...
🛑  1 nodes stopped.
```

```docker ps -a```
```
CONTAINER ID   IMAGE                                 COMMAND                  CREATED        STATUS                        PORTS     NAMES
d7e91d1e053d   gcr.io/k8s-minikube/kicbase:v0.0.27   "/usr/local/bin/entr…"   2 weeks ago    Exited (137) 52 seconds ago             minikube
```

minikube start:
```minukube start```
```
😄  minikube v1.23.1 on Debian 11.0
✨  Using the docker driver based on existing profile
👍  Starting control plane node minikube in cluster minikube
🚜  Pulling base image ...
🔄  Restarting existing docker container for "minikube" ...
🐳  Preparing Kubernetes v1.22.1 on Docker 20.10.8 ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
    ▪ Using image k8s.gcr.io/ingress-nginx/kube-webhook-certgen:v1.0
    ▪ Using image k8s.gcr.io/ingress-nginx/kube-webhook-certgen:v1.0
    ▪ Using image k8s.gcr.io/ingress-nginx/controller:v1.0.0-beta.3
🔎  Verifying ingress addon...
🌟  Enabled addons: default-storageclass, storage-provisioner, ingress
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
```

```villaraco@pc06:~/treball/dev/kubernetes-deploy-with-minikube$ docker exec minikube ls -l /data/mariadb-data```
```
total 123332
-rw-rw---- 1 999 docker    417792 Oct  3 17:55 aria_log.00000001
-rw-rw---- 1 999 docker        52 Oct  3 17:55 aria_log_control
-rw-rw---- 1 999 docker         9 Oct  3 17:59 ddl_recovery.log
drwx------ 2 999 docker      4096 Oct  3 17:45 flask_app
-rw-rw---- 1 999 docker       946 Oct  3 17:55 ib_buffer_pool
-rw-rw---- 1 999 docker 100663296 Oct  3 17:59 ib_logfile0
-rw-rw---- 1 999 docker  12582912 Oct  3 17:55 ibdata1
-rw-rw---- 1 999 docker  12582912 Oct  3 17:59 ibtmp1
-rw-rw---- 1 999 docker         0 Oct  3 17:44 multi-master.info
drwx------ 2 999 docker      4096 Oct  3 17:45 mysql
drwx------ 2 999 docker      4096 Oct  3 17:44 performance_schema
drwx------ 2 999 docker     12288 Oct  3 17:44 sys
```

> It's quite logic as minikube container was just stopped. Mariadb data directory won't be keeped after a ```minikube delete``` as minukube container is deleted.


## Testing PV after contextname is deleted and recreated

With an inicial position where contextname is created by ```00-startAll.sh``` and tested we have access to mariadb throught user_flask_app:

```kubectl exec -i pod/mariadb-6477d969b4-rqx4g -- /bin/sh -c 'mysql -u user_flask_app -psecret --skip-column-names' < mariadb/selectDatabaseTime.sql ```
```
2021-10-03 18:11:05
```

```99-stopAll.sh```

```kubectl get namespaces```
```
NAME              STATUS   AGE
default           Active   14d
ingress-nginx     Active   14d
kube-node-lease   Active   14d
kube-public       Active   14d
kube-system       Active   14d
```

```kubectl exec -i pod/mariadb-6477d969b4-4c9nh -- /bin/sh -c 'mysql -u user_flask_app -psecret --skip-column-names' < mariadb/selectDatabaseTime.sql ```
```
2021-10-03 18:11:05
```

> Please note mariadb pod name change.

