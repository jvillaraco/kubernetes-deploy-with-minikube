# 00-startAll.sh

kubectl create -f 01-app-namespace.yaml
kubectl create -f 02-app-mariadb-pv.yaml
kubectl create -f 03-app-mariadb-pvc.yaml
kubectl create -f 04-mariadb-deployment.yaml
kubectl create -f 05-mariadb-serviceExternal.yaml
kubectl create -f 06-flask-deployment.yaml
kubectl create -f 07-flask-app-serviceExternal.yaml
kubectl create -f 08-flask-app-ingress.yaml


echo "List everything in default namespace"
echo "------------------------------------"
kubectl get all,pv,pvc,ingress
echo; echo

echo "List everything in app namespace"
echo "--------------------------------"
kubectl get all,pv,pvc,ingress --namespace app
echo; echo