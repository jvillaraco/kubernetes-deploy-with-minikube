# kubernetes deploy with minikube
**Kubernetes app deploy with Minikube (over docker containers)**

# Project definition
## Objective
1. App developing and deploying must be done to test a Kubernetes deploying scenario with an app and BD as learned in "Kubernetes Introduction course"from EDX and Linux Foudation (https://learning.edx.org/course/course-v1:LinuxFoundationX+LFS158x+3T2020).
2. Should be done with all config options: Authentication, Authorization, Admission Control, Services, Volumes, ConfigMaps, Secrets, Ingress, Quota and Limits Management, Autoscaling, Jobs and CronJobs, DaemonSets, StatefulSets, Network Policies, 

## Exclutions
1. Ansible won't be used as this developing and deploying is focused in kubernetes

## Requirements
1. Must be done on a kubernetes 3-cluster simulation (Minikube)
2. App will developed with Flask
3. 3-tier app with: Web server + app server + BD
4. Definitions script files will be done with yaml format not json

## Design
1. 1 Namespace for app different than default
2. 3 nodes (or dockers in Minikube)

# Actual project situation
1. Namespace has been created
2. Stateful mariadb BD has been created and tested with a unique replica
3. Created flask image with nginx and loaded to docker hub in https://hub.docker.com/repository/docker/jvillaraco/flask
4. Created bash scripts to build and run image
5. External service for flask-app created
6. External final service for http://flask-app.jvillaraco.home (hostname and domain defined in /etc/hosts) configured and deployed throught an ingress set up.
7. Incident with ingress endpoint IP assign has been solved in https://gitlab.com/jvillaraco/kubernetes-deploy-with-minikube/-/issues/1
8. Created database and granted user access
9. Tested persistence in PV. Documented in [mariadb/readme.md](/mariadb/readme.md)

# Directories
Which is the pourpose for every directory
## Docker/
Maintains all files and config needed for building flask-app docker image

## admin/
Keeps all admin script to manage minikube and all other services

# TODO
1. Use secrets to keep passwords safe. Specifically, secret should be used with root db user
2. Create reverse proxy layer with Ingress addon for Kubernetes
3. Upgrade flask-app to use a volume and BD access
4. Increase replica number for 2 o 3 nodes
5. Define some diferents final web sites.
6. Define some diferents endpoints within some site.
7. Make DB data persistent between infraestructure recreations

# Flask image
(work in progress with it)

Dockerfile (and aux files nginx.conf and app.ini) has been got from https://hub.docker.com/r/izone/flask/ (thank you izone).

## Image build
```
docker build -t jvillaraco/flask:nginx .
```

## Docker Flask image test
```
docker run --rm --name app \   
-p 8080:80 \
-e PYTHONPATH=/app \
-e UWSGI_MODULE="app:app" \
-ti jvillaraco/flask:nginx
```
